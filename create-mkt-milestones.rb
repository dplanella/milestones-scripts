#!/usr/bin/env ruby

# MIT License
# 
# Copyright (c) 2020 David Planella <dplanella@gitlab.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Creates a set of marketing-wide biweekly milestones for a given week range

require 'date'
require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'gitlab'
end

# Configure the GitLab API client
Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4'
    config.private_token  = ENV['GITLAB_API_PRIVATE_TOKEN']
end

year = 2021
start_week = 2
end_week = 8
group = 'gitlab-com'

(start_week..end_week).step(2) do |week|
  iteration_start_date = Date.commercial(year, week - 1, 1)
  iteration_due_date = Date.commercial(year, week, 7)
  iteration_title = "Mktg: #{iteration_due_date}"
  iteration_description = "Marketing biweekly iteration, week #{week}. Ending on #{iteration_due_date}."

  milestone = Gitlab.create_group_milestone(
    group,
    iteration_title,
    description: iteration_description, 
    start_date: "#{iteration_start_date}",
    due_date: "#{iteration_due_date}"
  )
end