#!/usr/bin/env ruby

# MIT License
# 
# Copyright (c) 2020 David Planella <dplanella@gitlab.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Renames Corporate Marketing milestones to new nomenclature for marketing-wide
# milestones using the ISO date format

require 'date'
require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'gitlab'
end

# Configure the GitLab API client
Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4'
    config.private_token  = ENV['GITLAB_API_PRIVATE_TOKEN']
end

# Fetch active group milestones defined at the 'gitlab-com' group level
# which start with the 'Fri:' string in the title. These are effectively
# the current Corporate Marketing milestones.
milestones = Gitlab.group_milestones('gitlab-com', state: 'active', search: 'Fri:')

# Rename the milestone titles to be "Marketing: YYYY-MM-DD"
count = 0
milestones.auto_paginate do |milestone|
    title_orig = milestone.title
    title_new = "Marketing: " + Date.parse(milestone.title).to_s
    count += 1
    # milestone.title = title_new
    puts count.to_s + ". " + title_orig + " => " + title_new
end
